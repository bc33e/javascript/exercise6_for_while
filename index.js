// BÀI1
document.getElementById("btn-nguyen-duong").onclick = function () {
  var num = 0;
  var minNguyenDuong = 0;
  while (num < 10000) {
    minNguyenDuong++;

    num = minNguyenDuong + num;
  }
  console.log("minNguyenDuong: ", minNguyenDuong);
  document.getElementById(
    "ket-qua-1"
  ).innerHTML = `Số nguyên dương nhỏ nhất: ${minNguyenDuong}`;
};

// BÀI 2
document.getElementById("btn-tinh-tong").onclick = function () {
  var soX = document.getElementById("txt-so-x").value * 1;
  var soN = document.getElementById("txt-so-n").value * 1;
  console.log({ soX, soN });

  var tongSo = 0;
  var soMu = 1;
  for (var i = 1; i <= soN; i++) {
    soMu = soMu * soX;
    tongSo = tongSo + soMu;
  }
  console.log("tongSo: ", tongSo);
  document.getElementById("ket-qua-2").innerHTML = `Tổng: ${tongSo}`;
};

// BÀI 3
document.getElementById("btn-giai-thua").onclick = function () {
  var soNhapVao = document.getElementById("txt-so-m").value * 1;
  console.log("soNhapVao: ", soNhapVao);

  var giaiThua = 1;

  for (var i = 1; i <= soNhapVao; i++) {
    giaiThua = giaiThua * i;
  }
  console.log("giaiThua: ", giaiThua);
  document.getElementById("ket-qua-3").innerHTML = `Giai thừa: ${giaiThua}`;
};

// BÀI 4
function doiMau() {
  document.getElementById("ket-qua-4").style.display = "block";
  var theDiv = document.getElementsByClassName("bai4");
  for (var i = 0; i < theDiv.length; i++) {
    if ((i + 1) % 2 == 0) {
      theDiv[i].style.background = "red";
    } else {
      theDiv[i].style.background = "blue";
    }
  }
}

// BÀI 5
function kiemTraSo(number) {
  var kiemTra = true;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      kiemTra = false;
      break;
    }
  }
  return kiemTra;
}
document.getElementById("btn-so-nguyen-to").onclick = function () {
  var soNguyenTo = document.getElementById("txt-so-nguyen-to").value * 1;
  console.log("soNguyenTo: ", soNguyenTo);
  var ketQua = "";
  for (var i = 2; i <= soNguyenTo; i++) {
    var kiemTra = kiemTraSo(i);
    if (kiemTra) {
      ketQua += i + " ";
    }
  }
  console.log("ketQua: ", ketQua);
  document.getElementById("ket-qua-5").innerHTML = `Số nguyên tố: ${ketQua}`;
};
